fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	const title = data.map(data => { 
		return `${data.title}`
	})
	console.log(title)
})

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((data) => { 
	console.log(data)});

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'New title',
			body: 'mahabang espanol!',
			userId: 300
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated post",
		body: "Hello again",
		userId: 1,
		description: "updated status is this??",
		dateCompleted: "04-01-22",
		status: "updated status again??"
	})
})
.then(res => res.json())
.then(data => console.log(data))
 
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated post"
	})
})
.then(res => res.json())
.then(data => console.log(data))

